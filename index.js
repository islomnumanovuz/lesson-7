let second = 00;
let minute = 00;
let appendSec = document.getElementById("second");
let appendMin = document.getElementById("minute");
let btnStart = document.getElementById("start");
let btnStop = document.getElementById("pause");
let btnReset = document.getElementById("reset");
let time = document.getElementById("time");
let interval;

function timer() {
  second++;
  console.log(minute, second);
  if (second < 60) {
    appendSec.textContent = "0" + second;
  }
  if (second >= 10) {
    appendSec.textContent = second;
  }
  if (second == 60) {
    minute++;
    appendMin.textContent = "0" + minute;
    second = 00;
    appendSec.textContent = second;
  }
}
btnStart.onclick = function () {
  if (localStorage.length) {
    appendSec.textContent = localStorage.getItem("second");
    appendMin.textContent = localStorage.getItem("minute");
    second = parseInt(localStorage.getItem("second"));
    minute = parseInt(localStorage.getItem("minute"));
  }
  interval = setInterval(timer, 1000);
};
btnStop.onclick = function () {
  clearInterval(interval);
  localStorage.setItem("second", second);
  localStorage.setItem("minute", minute);
};
btnReset.onclick = function () {
  appendMin.textContent = "0" + 0;
  appendSec.textContent = "0" + 0;
  second = 00;
  minute = 00;
  window.localStorage.clear();
};
